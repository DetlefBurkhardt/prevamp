import groovy.json.*
import groovy.text.*

def restResponse = new File("types.json") 
def js = """${restResponse.text}"""
def data = new JsonSlurper().parseText(js) 

def totalCount = data.size
def count = 0

def frontdelim = "---\n"
println "Start"
new File("docs/types").mkdirs() 
data.each {
    def shortDesc = it["Description"].tokenize(".")[0] ?: ''
    
    def content = ""
    if (shortDesc.size() > 0) {
        shortDesc += "."

        if (it["EnumValues-Or-Examples"].size() > 0) {
            content = "Values: " + it["EnumValues-Or-Examples"] + "\n\n" 
        }    
        content +=  it["Description"]
    } else {
        content = "tbd."
        shortDesc = "tbd."
    }
 
    def md = frontdelim
    md += "title: " + it["Name"] + "\n"
    md += "categories: [\"" + it["Based-On"] + "\"]\n"
    md += "tags: [\"tbd\", \"" + it["Name"].toLowerCase() + "\", \"" + it["Based-On"].toLowerCase() +"\"] \n"
    md += "date: " + new Date().format( 'yyyy-MM-dd' )  + "\n"
    md += "\n"

    md += "description: \"" + shortDesc + "\"\n"
    md += "lastEdit: " + new Date().format( 'yyyy-MM-dd' )  + "\n"
    md += "lastEditor: generator" + "\n"
    md += frontdelim + "\n"

    md += content.trim() + "\n"
    //md += "\n-------------------------------------------<eof>\n\n"
    
    def outputFile = "docs\\types\\" + it["Name"] + ".md"
    def output = new File(outputFile)
    println count++ + " " + outputFile
    //println md
    output.write md


}

println "-------------------------------------------"
println "total:\t" + totalCount
println "-------------------------------------------"


File file