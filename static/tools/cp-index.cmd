copy ..\..\content\docs\types\_index.md docs\types
copy ..\..\content\docs\entities\_index.md docs\entities
copy ..\..\content\docs\attributes\_index.md docs\attributes
copy ..\..\content\docs\attributes\cluster-attributes\_index.md docs\attributes\cluster-attributes
copy ..\..\content\docs\attributes\edge-attributes\_index.md docs\attributes\edge-attributes
copy ..\..\content\docs\attributes\graph-attributes\_index.md docs\attributes\graph-attributes
copy ..\..\content\docs\attributes\node-attributes\_index.md docs\attributes\node-attributes