import groovy.json.*
import groovy.text.*

def restResponse = new File("attributes.json") //later via Rest-Call .parse(URI)
def js = """${restResponse.text}"""
def data = new JsonSlurper().parseText(js) 

def totalCount = data.size

def graphCount = data.count{it["Cat"] == "G"}
def nodeCount = data.count{it["Cat"] == "N"}
def edgeCount = data.count{it["Cat"] == "E"}
def clusterCount = data.count{it["Cat"] == "C"}
def count = 0

def frontdelim = "---\n"
//def md = ""

def catMap = [G:'graph', N:'node', E:'edge', C:'cluster']
    
new File("docs/attributes/graph-attributes").mkdirs() 
new File("docs/attributes/node-attributes").mkdirs() 
new File("docs/attributes/edge-attributes").mkdirs() 
new File("docs/attributes/cluster-attributes").mkdirs() 

data.each {
    def shortDesc = it["Description"].tokenize(".")[0] + "."
    //def content = it["Description"].minus(shortDesc)
    def content = it["Description"]
    def categ = catMap[it["Cat"]]
    //def allTypes = it["Type"].tokenize(",")
    def myType = it["Type"].replaceAll(',','\", \"')
 
    
    def md = frontdelim
    md += "title: " + it["Name"] + "\n"
    md += "categories:  [\"" + categ + "\"]\n"
    md += "tags: [\"tbd\", \"attribute\", \"" + categ.toLowerCase() + "\", \"" + it["Name"].toLowerCase() + "\", \"" + myType.toLowerCase() + "\"]\n"
    md += "date: " + new Date().format( 'yyyy-MM-dd' )  + "\n"
    md += "\n"

    md += "description: \"" + shortDesc + "\"\n"
    md += "types: [\"" + myType + "\"]\n"
    md += "default: \"" + it["Default"] + "\"\n"
    md += "min: \"" + it["Min"] + "\"\n"
    md += "restrictions: \"" + it["Restrictions"] + "\"\n"
    md += "examples: \"" + it["Examples"] + "\"\n"
    md += "lastEdit: " + new Date().format( 'yyyy-MM-dd' )  + "\n"
    md += "lastEditor: generator" + "\n"
    md += frontdelim + "\n"
    md += content.trim() + "\n"
    //md += "\n-------------------------------------------<eof>\n\n"
    
    def outputFile = "docs\\attributes\\" + categ + "-attributes\\" + it["Name"] + ".md"
    def output = new File(outputFile)
    output.write md
    //println md

    println count++ + " " + outputFile
}

println "-------------------------------------------"
println "total:\t" + totalCount
println "graph:\t" + graphCount
println "node:\t" + nodeCount
println "edge:\t" + edgeCount
println "clstr:\t" + clusterCount
println "-------------------------------------------"


//File file
//print md