import groovy.json.*
import groovy.text.*

def restResponse = new File("entities.json") 
def js = """${restResponse.text}"""
def data = new JsonSlurper().parseText(js) 

def totalCount = data.size
def count = 0

def frontdelim = "---\n"


new File("docs/entities").mkdirs() 
println "Start"
data.each {
    def shortDesc = it["Description"].tokenize(".")[0] ?: ''
    
    def content = ""
    if (shortDesc.size() > 0) {
        shortDesc += "."
        content = "Shaping: " + it["Characteristics"] + "\n\n" + it["Description"]
    }
    //def shortDesc = it["Description"].tokenize(".")[0] + "."
    //def content = it["Description"].minus(shortDesc) + ""
 
    def md = frontdelim
    md += "title: " + it["Name"] + "\n"
    md += "categories: [\"entity\"]\n"
    md += "tags: [\"tbd\", \"entity\", \"" + it["Level"] + "\", \"" + it["Name"].toLowerCase() +  "\"]\n"
    
    md += "date: " + new Date().format( 'yyyy-MM-dd' )  + "\n"
    md += "\n"

    md += "description: \"" + shortDesc + "\"\n"
    md += "lastEdit: " + new Date().format( 'yyyy-MM-dd' )  + "\n"
    md += "lastEditor: generator" + "\n"
    md += frontdelim + "\n"

    md += content.trim() + "\n"
    //md += "\n-------------------------------------------<eof>\n\n"
    
    def outputFile = "docs\\entities\\" + it["Name"].toLowerCase() + ".md"
    def output = new File(outputFile)
    println count++ + " " + outputFile
    //println md
    output.write md


}

println "-------------------------------------------"
println "total:\t" + totalCount
println "-------------------------------------------"


