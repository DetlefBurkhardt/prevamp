---
title: Subgraph
categories: ["entity"]
tags: ["tbd", "entity", "primary", "subgraph"]
tags-weight: 50
date: 2018-04-15

description: "Subgraph is a grouping for entities."
lastEdit: 2018-04-15
lastEditor: generator
---

Shaping: cluster, non-cluster

Subgraph is a grouping for entities. With a subgraph it is possible to group entities like nodes, edges or other subgraphs together. Only if the name of a subgraph begins with "Cluster" this grouping becomes a spatial dimension.  

2do: 1-2 Examples.  
2do: Link to Cluster
