---
title: Label
categories: ["entity"]
tags: ["tbd", "entity", "secondary", "label"]
tags-weight: 100
date: 2018-04-15

description: "Describes entities with textual informations."
lastEdit: 2018-04-15
lastEditor: generator
---

Shaping: text, html

Describes entities with textual informations. Because of the vital meaning of an label and their amount of attributes as record, text oder html and their constrains, it makes sense to define a label as an entity, even it is technically just an attribute.  
2do: More Descriptions & Explanations.  
2do: 5-6 Examples.  

2do: Link/Merge Label-Attribute, List of Label-Specific Attributes (evt. develop its owhn attribute-category)
