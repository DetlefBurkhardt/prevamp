---
title: Cluster
categories: ["entity"]
tags: ["tbd", "entity", "primary", "cluster"]
tags-weight: 40
date: 2018-04-15

description: "Cluster is a special characteristic of a subgraph, which leads to a spatial grouping."
lastEdit: 2018-04-15
lastEditor: generator
---

Shaping: spatial grouping

Cluster is a special characteristic of a subgraph, which leads to a spatial grouping. It is described with "Subgraph Cluster[_Postfix] {...}". Without the "Cluster"-Prefix it is just a non-spatial grouping, see subgraph.   Cluster can have labels, style and can contain nodes, edges or other subgraphs.  

2do: 1-2 Cluster Examples.  
2do: Link to Cluster-Attributes.
