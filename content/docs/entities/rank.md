---
title: Rank
categories: ["entity"]
tags: ["tbd", "entity", "secondary", "rank"]
tags-weight: 110
date: 2018-04-15

description: "With rank you can influence the spatial disposal of nodes while calculating a graph."
lastEdit: 2018-04-15
lastEditor: generator
---

Shaping: same, min, source, max, sink

With rank you can influence the spatial disposal of nodes while calculating a graph. Without an individual setting the rank of a node in a calculated graph follows the number of edges to the root. To change this default rank behaviour you can use the attributes same, min, source, max, sink.  

2do: Examples for all 5 Attributes.  
2do: Link/Merge to Rank Attrbute (evt. merge them here)
