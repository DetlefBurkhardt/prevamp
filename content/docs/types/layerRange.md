---
title: layerRange
categories: ["List of Strings"]
tags: ["tbd", "layerrange", "list of strings"] 
date: 2018-04-15

description: "Specifies a list of layers defined by the layers attribute."
lastEdit: 2018-04-15
lastEditor: generator
---

Specifies a list of layers defined by the layers attribute.
