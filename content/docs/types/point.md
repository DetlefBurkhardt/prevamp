---
title: point
categories: ["Coordinate"]
tags: ["tbd", "point", "coordinate"] 
date: 2018-04-15

description: "Representing the point (x,y)."
lastEdit: 2018-04-15
lastEditor: generator
---

Values: %f, %f('!')? 

Representing the point (x,y). The optional '!' indicates the node position should not change (input-only). If dim is 3, point may also have the format "%f,%f,%f('!')?" to represent the point (x,y,z).
