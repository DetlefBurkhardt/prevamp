---
title: doubleList
categories: ["List of Doubles"]
tags: ["tbd", "doublelist", "list of doubles"] 
date: 2018-04-15

description: "A colon-separated list of doubles: '%f(:%f)*' where each %f is a double."
lastEdit: 2018-04-15
lastEditor: generator
---

A colon-separated list of doubles: '%f(:%f)*' where each %f is a double.
