---
title: dirType
categories: ["Enum"]
tags: ["tbd", "dirtype", "enum"] 
date: 2018-04-15

description: "For an edge T -> H."
lastEdit: 2018-04-15
lastEditor: generator
---

Values: forward, back, both, none

For an edge T -> H.
