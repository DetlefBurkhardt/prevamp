---
title: shape
categories: ["String"]
tags: ["tbd", "shape", "string"] 
date: 2018-04-15

description: "A string specifying the shape of a node."
lastEdit: 2018-04-15
lastEditor: generator
---

A string specifying the shape of a node. There are three main types of shapes : polygon-based, record-based and user-defined. The record-based shape has largely been superseded and greatly generalized by HTML-like labels. That is, instead of using shape=record, one might consider using shape=none and an HTML-like label.
