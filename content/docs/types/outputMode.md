---
title: outputMode
categories: ["Enum"]
tags: ["tbd", "outputmode", "enum"] 
date: 2018-04-15

description: "These specify the order in which nodes and edges are drawn in concrete output."
lastEdit: 2018-04-15
lastEditor: generator
---

Values: breadthfirst, nodesfirst, edgesfirst

These specify the order in which nodes and edges are drawn in concrete output.
