---
title: addDouble
categories: ["Double"]
tags: ["tbd", "adddouble", "double"] 
date: 2018-04-15

description: "A double with an optional prefix '+'."
lastEdit: 2018-04-15
lastEditor: generator
---

A double with an optional prefix '+'.
