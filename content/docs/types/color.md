---
title: color
categories: ["String"]
tags: ["tbd", "color", "string"] 
date: 2018-04-15

description: "Colors can be specified using one of four formats: RGB, RGBA, HSV, string."
lastEdit: 2018-04-15
lastEditor: generator
---

Values: #606060, #6060605F, blue, green

Colors can be specified using one of four formats: RGB, RGBA, HSV, string.
