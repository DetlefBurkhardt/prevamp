---
title: portPos
categories: ["Enum"]
tags: ["tbd", "portpos", "enum"] 
date: 2018-04-15

description: "Modifier indicating where on a node an edge should be aimed."
lastEdit: 2018-04-15
lastEditor: generator
---

Modifier indicating where on a node an edge should be aimed. It has the form portname(:compass_point)? or compass_point.
