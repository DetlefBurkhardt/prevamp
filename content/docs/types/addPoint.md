---
title: addPoint
categories: ["Double"]
tags: ["tbd", "addpoint", "double"] 
date: 2018-04-15

description: "A point with an optional prefix '+'."
lastEdit: 2018-04-15
lastEditor: generator
---

A point with an optional prefix '+'.
