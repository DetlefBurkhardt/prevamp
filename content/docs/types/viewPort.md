---
title: viewPort
categories: ["Coordinates"]
tags: ["tbd", "viewport", "coordinates"] 
date: 2018-04-15

description: "It specifies a viewport for the final image."
lastEdit: 2018-04-15
lastEditor: generator
---

Values: W,H,Z,x,y or W,H,Z,N

It specifies a viewport for the final image.
