---
title: area
categories:  ["node"]
tags: ["tbd", "attribute", "node", "area", "double"]
date: 2018-04-15

description: "Indicates the preferred area for a node when laid out by patchwork."
types: ["double"]
default: "1.0"
min: ">0"
restrictions: "patchwork only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Indicates the preferred area for a node when laid out by patchwork.
