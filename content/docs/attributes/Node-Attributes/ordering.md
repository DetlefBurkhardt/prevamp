---
title: ordering
categories:  ["node"]
tags: ["tbd", "attribute", "node", "ordering", "string"]
date: 2018-04-15

description: "If the value of the attribute is 'out', then the outedges of a node, that is, edges with the node as its tail node, must appear left-to-right in the same order in which they are defined in the input."
types: ["string"]
default: "n/a"
min: ""
restrictions: "dot only"
examples: "awilliams.gv"
lastEdit: 2018-04-15
lastEditor: generator
---

If the value of the attribute is 'out', then the outedges of a node, that is, edges with the node as its tail node, must appear left-to-right in the same order in which they are defined in the input. If the value of the attribute is 'in', then the inedges of a node must appear left-to-right in the same order in which they are defined in the input.
