---
title: fontcolor
categories:  ["node"]
tags: ["tbd", "attribute", "node", "fontcolor", "color"]
date: 2018-04-15

description: "Color used for text."
types: ["color"]
default: "black"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Color used for text.
