---
title: imagepos
categories:  ["node"]
tags: ["tbd", "attribute", "node", "imagepos", "string"]
date: 2018-04-15

description: "Attribute controlling how an image is positioned within its containing node."
types: ["string"]
default: "mc"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Attribute controlling how an image is positioned within its containing node. This only has an effect when the image is smaller than the containing node. The default is to be centered both horizontally and vertically.
