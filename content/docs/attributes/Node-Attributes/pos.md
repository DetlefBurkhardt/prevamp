---
title: pos
categories:  ["node"]
tags: ["tbd", "attribute", "node", "pos", "point", "splinetype"]
date: 2018-04-15

description: "Position of node."
types: ["point", "splineType"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Position of node. For nodes, the position indicates the center of the node. On output, the coordinates are in points.
