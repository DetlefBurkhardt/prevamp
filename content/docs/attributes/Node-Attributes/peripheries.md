---
title: peripheries
categories:  ["node"]
tags: ["tbd", "attribute", "node", "peripheries", "int"]
date: 2018-04-15

description: "Set number of peripheries used in polygonal shapes."
types: ["int"]
default: "shape default"
min: "0"
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Set number of peripheries used in polygonal shapes. Note that user-defined shapes are treated as a form of box shape, so the default peripheries value is 1 and the user-defined shape will be drawn in a bounding rectangle. Setting peripheries=0 will turn this off.
