---
title: vertices
categories:  ["node"]
tags: ["tbd", "attribute", "node", "vertices", "pointlist"]
date: 2018-04-15

description: "If the input graph defines this attribute, the node is polygonal, and output is dot or xdot, this attribute provides the coordinates of the vertices of the node's polygon, in inches."
types: ["pointList"]
default: "n/a"
min: ""
restrictions: "write only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If the input graph defines this attribute, the node is polygonal, and output is dot or xdot, this attribute provides the coordinates of the vertices of the node's polygon, in inches. If the node is an ellipse or circle, the samplepoints attribute affects the output.
