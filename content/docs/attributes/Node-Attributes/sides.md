---
title: sides
categories:  ["node"]
tags: ["tbd", "attribute", "node", "sides", "int"]
date: 2018-04-15

description: "Number of sides if shape=polygon."
types: ["int"]
default: "4"
min: "0"
restrictions: ""
examples: "abstract.gv"
lastEdit: 2018-04-15
lastEditor: generator
---

Number of sides if shape=polygon.
