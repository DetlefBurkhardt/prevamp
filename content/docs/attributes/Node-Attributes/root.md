---
title: root
categories:  ["node"]
tags: ["tbd", "attribute", "node", "root", "string", "bool"]
date: 2018-04-15

description: "This specifies nodes to be used as the center of the layout and the root of the generated spanning tree."
types: ["string", "bool"]
default: "FALSE"
min: ""
restrictions: "circo, twopi only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

This specifies nodes to be used as the center of the layout and the root of the generated spanning tree.
