---
title: regular
categories:  ["node"]
tags: ["tbd", "attribute", "node", "regular", "bool"]
date: 2018-04-15

description: "If true, force polygon to be regular, i."
types: ["bool"]
default: "FALSE"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If true, force polygon to be regular, i.e., the vertices of the polygon will lie on a circle whose center is the center of the node.
