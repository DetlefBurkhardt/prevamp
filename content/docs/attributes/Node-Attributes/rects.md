---
title: rects
categories:  ["node"]
tags: ["tbd", "attribute", "node", "rects", "rect"]
date: 2018-04-15

description: "Rectangles for fields of records, in points."
types: ["rect"]
default: "n/a"
min: ""
restrictions: "write only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Rectangles for fields of records, in points.
