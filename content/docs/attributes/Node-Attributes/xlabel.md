---
title: xlabel
categories:  ["node"]
tags: ["tbd", "attribute", "node", "xlabel", "lblstring"]
date: 2018-04-15

description: "External label for a node."
types: ["lblString"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

External label for a node. The label will be placed outside of the node but near it.
