---
title: distortion
categories:  ["node"]
tags: ["tbd", "attribute", "node", "distortion", "double"]
date: 2018-04-15

description: "Distortion factor for shape=polygon."
types: ["double"]
default: "0.0"
min: "-100.0"
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Distortion factor for shape=polygon. Positive values cause top part to be larger than bottom; negative values do the opposite.
