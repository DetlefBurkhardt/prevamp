---
title: z
categories:  ["node"]
tags: ["tbd", "attribute", "node", "z", "double"]
date: 2018-04-15

description: "Use pos attribute, along with dimen and/or dim to specify dimensions (Deprecated)."
types: ["double"]
default: "0.0"
min: "-MAXFLOAT, -1000"
restrictions: "Deprecated"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Use pos attribute, along with dimen and/or dim to specify dimensions (Deprecated).
