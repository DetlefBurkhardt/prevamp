---
title: skew
categories:  ["node"]
tags: ["tbd", "attribute", "node", "skew", "double"]
date: 2018-04-15

description: "Skew factor for shape=polygon."
types: ["double"]
default: "0.0"
min: "-100.0"
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Skew factor for shape=polygon. Positive values skew top of polygon to right; negative to left.
