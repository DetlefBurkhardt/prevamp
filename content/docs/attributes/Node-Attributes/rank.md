---
title: rank
categories:  ["node"]
tags: ["tbd", "attribute", "node", "rank", "ranktype"]
date: 2018-04-15

description: "Rank constraints on the nodes in a (sub)graph."
types: ["rankType"]
default: "n/a"
min: ""
restrictions: "dot only"
examples: "world.gv"
lastEdit: 2018-04-15
lastEditor: generator
---

Rank constraints on the nodes in a (sub)graph. If rank='same', all nodes are placed on the same rank. If rank='min', all nodes are placed on the minimum rank.
