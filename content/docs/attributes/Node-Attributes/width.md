---
title: width
categories:  ["node"]
tags: ["tbd", "attribute", "node", "width", "double"]
date: 2018-04-15

description: "Width of node, in inches."
types: ["double"]
default: "0.75"
min: "0.01"
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Width of node, in inches. This is taken as the initial, minimum width of the node. If fixedsize is true, this will be the final width of the node. Otherwise, if the node label requires more width to fit, the node's width will be increased to contain the label.
