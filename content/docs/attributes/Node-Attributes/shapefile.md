---
title: shapefile
categories:  ["node"]
tags: ["tbd", "attribute", "node", "shapefile", "string"]
date: 2018-04-15

description: "If defined, shapefile specifies a file containing user-supplied node content (Deprecated)."
types: ["string"]
default: "n/a"
min: ""
restrictions: "Deprecated"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If defined, shapefile specifies a file containing user-supplied node content (Deprecated).
