---
title: comment
categories:  ["node"]
tags: ["tbd", "attribute", "node", "comment", "string"]
date: 2018-04-15

description: "Comments are inserted into output."
types: ["string"]
default: "n/a"
min: ""
restrictions: "Device-dependent"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Comments are inserted into output.
