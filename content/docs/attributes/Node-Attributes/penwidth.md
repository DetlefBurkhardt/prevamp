---
title: penwidth
categories:  ["node"]
tags: ["tbd", "attribute", "node", "penwidth", "double"]
date: 2018-04-15

description: "Specifies the width of the pen, in points, used to draw lines and curves, including the boundaries of edges and clusters."
types: ["double"]
default: "1.0"
min: "0.0"
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Specifies the width of the pen, in points, used to draw lines and curves, including the boundaries of edges and clusters. The value is inherited by subclusters. It has no effect on text.
