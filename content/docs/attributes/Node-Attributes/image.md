---
title: image
categories:  ["node"]
tags: ["tbd", "attribute", "node", "image", "string"]
date: 2018-04-15

description: "Gives the name of a file containing an image to be displayed inside a node."
types: ["string"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Gives the name of a file containing an image to be displayed inside a node. The image file must be in one of the recognized formats, typically JPEG, PNG, GIF, BMP, SVG or Postscript, and be able to be converted into the desired output format.
