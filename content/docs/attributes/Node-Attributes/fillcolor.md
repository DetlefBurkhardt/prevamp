---
title: fillcolor
categories:  ["node"]
tags: ["tbd", "attribute", "node", "fillcolor", "color", "colorlist"]
date: 2018-04-15

description: "Color used to fill the background of a node assuming style=filled."
types: ["color", "colorList"]
default: "lightgrey"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Color used to fill the background of a node assuming style=filled. If fillcolor is not defined, color is used.
