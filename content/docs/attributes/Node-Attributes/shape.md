---
title: shape
categories:  ["node"]
tags: ["tbd", "attribute", "node", "shape", "shape"]
date: 2018-04-15

description: "Set the shape of a node."
types: ["shape"]
default: "ellipse"
min: ""
restrictions: ""
examples: "alf.gv"
lastEdit: 2018-04-15
lastEditor: generator
---

Set the shape of a node.
