---
title: orientation
categories:  ["node"]
tags: ["tbd", "attribute", "node", "orientation", "double"]
date: 2018-04-15

description: "Angle, in degrees, used to rotate polygon node shapes."
types: ["double"]
default: "0.0"
min: "360.0"
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Angle, in degrees, used to rotate polygon node shapes. For any number of polygon sides, 0 degrees rotation results in a flat base.
