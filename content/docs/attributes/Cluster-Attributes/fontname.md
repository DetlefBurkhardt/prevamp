---
title: fontname
categories:  ["cluster"]
tags: ["tbd", "attribute", "cluster", "fontname", "string"]
date: 2018-04-15

description: "Font used for text."
types: ["string"]
default: "Times-Roman"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Font used for text.
