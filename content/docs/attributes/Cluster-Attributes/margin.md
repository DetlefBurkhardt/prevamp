---
title: margin
categories:  ["cluster"]
tags: ["tbd", "attribute", "cluster", "margin", "double", "point"]
date: 2018-04-15

description: "This sets x and y margins in inches."
types: ["double", "point"]
default: "n/a"
min: ""
restrictions: "device-dependent"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

This sets x and y margins in inches. If the margin is a single double, both margins are set equal to the given value.
