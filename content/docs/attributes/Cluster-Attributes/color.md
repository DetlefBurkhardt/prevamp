---
title: color
categories:  ["cluster"]
tags: ["tbd", "attribute", "cluster", "color", "color", "colorlist"]
date: 2018-04-15

description: "Basic drawing color for graphics, not text."
types: ["color", "colorList"]
default: "black"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Basic drawing color for graphics, not text. For the latter, use the fontcolor attribute.
