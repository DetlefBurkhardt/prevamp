---
title: pencolor
categories:  ["cluster"]
tags: ["tbd", "attribute", "cluster", "pencolor", "color"]
date: 2018-04-15

description: "Color used to draw the bounding box around a cluster."
types: ["color"]
default: "black"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Color used to draw the bounding box around a cluster. If pencolor is not defined, color is used. If this is not defined, bgcolor is used. If this is not defined, the default is used.
