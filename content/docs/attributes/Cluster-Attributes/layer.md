---
title: layer
categories:  ["cluster"]
tags: ["tbd", "attribute", "cluster", "layer", "layerrange"]
date: 2018-04-15

description: "Specifies layers in which the cluster is present."
types: ["layerRange"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Specifies layers in which the cluster is present.
