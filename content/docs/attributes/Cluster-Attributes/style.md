---
title: style
categories:  ["cluster"]
tags: ["tbd", "attribute", "cluster", "style", "style"]
date: 2018-04-15

description: "Set style information for components of the cluster."
types: ["style"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Set style information for components of the cluster.
