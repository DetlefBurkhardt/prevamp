---
title: colorscheme
categories:  ["cluster"]
tags: ["tbd", "attribute", "cluster", "colorscheme", "string"]
date: 2018-04-15

description: "This attribute specifies a color scheme namespace."
types: ["string"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

This attribute specifies a color scheme namespace.
