---
title: URL
categories:  ["cluster"]
tags: ["tbd", "attribute", "cluster", "url", "escstring"]
date: 2018-04-15

description: "Hyperlinks incorporated into device-dependent output."
types: ["escString"]
default: "n/a"
min: ""
restrictions: "svg, postscript, map only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Hyperlinks incorporated into device-dependent output. At present, used in ps2, cmap, i*map and svg formats. For all these formats, URLs can be attached to nodes, edges and clusters. URL attributes can also be attached to the root graph in ps2, cmap and i*map formats. This serves as the base URL for relative URLs in the former, and as the default image map file in the latter.
