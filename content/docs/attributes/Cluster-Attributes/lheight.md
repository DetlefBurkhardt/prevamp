---
title: lheight
categories:  ["cluster"]
tags: ["tbd", "attribute", "cluster", "lheight", "double"]
date: 2018-04-15

description: "Height of cluster label in inches."
types: ["double"]
default: "n/a"
min: ""
restrictions: "write only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Height of cluster label in inches.
