---
title: sortv
categories:  ["cluster"]
tags: ["tbd", "attribute", "cluster", "sortv", "int"]
date: 2018-04-15

description: "If packmode indicates an array packing, this attribute specifies an insertion order among the components, with smaller values inserted first."
types: ["int"]
default: "0"
min: "0"
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If packmode indicates an array packing, this attribute specifies an insertion order among the components, with smaller values inserted first.
