---
title: arrowsize
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "arrowsize", "double"]
date: 2018-04-15

description: "Multiplicative scale factor for arrowheads."
types: ["double"]
default: "1.0"
min: "0.0"
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Multiplicative scale factor for arrowheads.
