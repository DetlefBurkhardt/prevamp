---
title: sametail
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "sametail", "string"]
date: 2018-04-15

description: "Edges with the same tail and the same sametail value are aimed at the same point on the tail."
types: ["string"]
default: "n/a"
min: ""
restrictions: "dot only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Edges with the same tail and the same sametail value are aimed at the same point on the tail. This has no effect on loops. Each node can have at most 5 unique sametail values. See limitation.
