---
title: labelURL
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "labelurl", "escstring"]
date: 2018-04-15

description: "If labelURL is defined, this is the link used for the label of an edge."
types: ["escString"]
default: "n/a"
min: ""
restrictions: "svg, map only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If labelURL is defined, this is the link used for the label of an edge. This value overrides any URL defined for the edge.
