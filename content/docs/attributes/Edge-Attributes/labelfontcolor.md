---
title: labelfontcolor
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "labelfontcolor", "color"]
date: 2018-04-15

description: "Color used for headlabel and taillabel."
types: ["color"]
default: "black"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Color used for headlabel and taillabel. If not set, defaults to edge's fontcolor.
