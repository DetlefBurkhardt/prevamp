---
title: href
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "href", "escstring"]
date: 2018-04-15

description: "Synonym for URL."
types: ["escString"]
default: "n/a"
min: ""
restrictions: "svg, postscript, map only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Synonym for URL.
