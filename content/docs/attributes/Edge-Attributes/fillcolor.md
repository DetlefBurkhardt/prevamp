---
title: fillcolor
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "fillcolor", "color", "colorlist"]
date: 2018-04-15

description: "Color used to fill the background of a filled arrowhead assuming style=filled."
types: ["color", "colorList"]
default: "black"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Color used to fill the background of a filled arrowhead assuming style=filled. If fillcolor is not defined, color is used.
