---
title: showboxes
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "showboxes", "int"]
date: 2018-04-15

description: "Print guide boxes in PostScript at the beginning of routesplines if 1, or at the end if 2."
types: ["int"]
default: "0"
min: "0"
restrictions: "dot, postscript only for debuging"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Print guide boxes in PostScript at the beginning of routesplines if 1, or at the end if 2. (Debugging)
