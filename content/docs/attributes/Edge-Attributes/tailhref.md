---
title: tailhref
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "tailhref", "escstring"]
date: 2018-04-15

description: "Synonym for tailURL."
types: ["escString"]
default: "n/a"
min: ""
restrictions: "svg, map only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Synonym for tailURL.
