---
title: arrowhead
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "arrowhead", "arrowtype"]
date: 2018-04-15

description: "Style of arrowhead on the head node of an edge."
types: ["arrowType"]
default: "normal"
min: ""
restrictions: ""
examples: "arr_none.gv, arrows.gv"
lastEdit: 2018-04-15
lastEditor: generator
---

Style of arrowhead on the head node of an edge. This will only appear if the dir attribute is 'forward' or 'both'. See the limitation.
