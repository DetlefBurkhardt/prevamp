---
title: edgeURL
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "edgeurl", "escstring"]
date: 2018-04-15

description: "If edgeURL is defined, this is the link used for the non-label parts of an edge."
types: ["escString"]
default: "n/a"
min: ""
restrictions: "svg, map only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If edgeURL is defined, this is the link used for the non-label parts of an edge. This value overrides any URL defined for the edge. Also, this value is used near the head or tail node unless overridden by a headURL or tailURL value, respectively. See limitation.
