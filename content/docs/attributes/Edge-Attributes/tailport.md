---
title: tailport
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "tailport", "portpos"]
date: 2018-04-15

description: "Indicates where on the tail node to attach the tail of the edge."
types: ["portPos"]
default: "center"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Indicates where on the tail node to attach the tail of the edge. See limitation.
