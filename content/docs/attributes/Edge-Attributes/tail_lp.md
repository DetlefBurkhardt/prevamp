---
title: tail_lp
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "tail_lp", "point"]
date: 2018-04-15

description: "Position of an edge's tail label, in points."
types: ["point"]
default: "n/a"
min: ""
restrictions: "write only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Position of an edge's tail label, in points. The position indicates the center of the label.
