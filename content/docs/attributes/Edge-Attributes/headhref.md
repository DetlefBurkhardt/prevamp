---
title: headhref
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "headhref", "escstring"]
date: 2018-04-15

description: "Synonym for headURL."
types: ["escString"]
default: "n/a"
min: ""
restrictions: "svg, map only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Synonym for headURL
