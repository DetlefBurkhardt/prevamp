---
title: labelfontsize
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "labelfontsize", "double"]
date: 2018-04-15

description: "Font size, in points, used for headlabel and taillabel."
types: ["double"]
default: "14.0"
min: "1.0"
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Font size, in points, used for headlabel and taillabel. If not set, defaults to edge's fontsize.
