---
title: labeldistance
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "labeldistance", "double"]
date: 2018-04-15

description: "Multiplicative scaling factor adjusting the distance that the headlabel(taillabel) is from the head(tail) node."
types: ["double"]
default: "1.0"
min: "0.0"
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Multiplicative scaling factor adjusting the distance that the headlabel(taillabel) is from the head(tail) node. The default distance is 10 points. See labelangle for more details.
