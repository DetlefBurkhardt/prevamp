---
title: lhead
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "lhead", "string"]
date: 2018-04-15

description: "Logical head of an edge."
types: ["string"]
default: "n/a"
min: ""
restrictions: "dot only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Logical head of an edge. When compound is true, if lhead is defined and is the name of a cluster containing the real head, the edge is clipped to the boundary of the cluster. See limitation.
