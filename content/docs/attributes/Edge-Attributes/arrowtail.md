---
title: arrowtail
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "arrowtail", "arrowtype"]
date: 2018-04-15

description: "Style of arrowhead on the tail node of an edge."
types: ["arrowType"]
default: "normal"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Style of arrowhead on the tail node of an edge. This will only appear if the dir attribute is 'back' or 'both'. See the limitation
