---
title: pos
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "pos", "point", "splinetype"]
date: 2018-04-15

description: "Position of spline control points."
types: ["point", "splineType"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Position of spline control points. On output, the coordinates are in points.
