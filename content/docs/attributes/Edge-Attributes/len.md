---
title: len
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "len", "double"]
date: 2018-04-15

description: "Preferred edge length, in inches."
types: ["double"]
default: "1.0(neato), 0.3(fdp)"
min: ""
restrictions: "fdp, neato only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Preferred edge length, in inches.
