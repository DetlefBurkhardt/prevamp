---
title: style
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "style", "style"]
date: 2018-04-15

description: "Set style information for components of the edge."
types: ["style"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Set style information for components of the edge.
