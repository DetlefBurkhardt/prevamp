---
title: ltail
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "ltail", "string"]
date: 2018-04-15

description: "Logical tail of an edge."
types: ["string"]
default: "n/a"
min: ""
restrictions: "dot only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Logical tail of an edge. When compound is true, if ltail is defined and is the name of a cluster containing the real tail, the edge is clipped to the boundary of the cluster. See limitation.
