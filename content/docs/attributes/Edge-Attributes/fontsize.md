---
title: fontsize
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "fontsize", "double"]
date: 2018-04-15

description: "Font size, in points, used for text."
types: ["double"]
default: "14.0"
min: "1.0"
restrictions: ""
examples: "crazy.gv"
lastEdit: 2018-04-15
lastEditor: generator
---

Font size, in points, used for text.
