---
title: labelfontname
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "labelfontname", "string"]
date: 2018-04-15

description: "Font used for headlabel and taillabel."
types: ["string"]
default: "Times-Roman"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Font used for headlabel and taillabel. If not set, defaults to edge's fontname.
