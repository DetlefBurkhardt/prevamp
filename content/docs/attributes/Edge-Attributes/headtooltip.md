---
title: headtooltip
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "headtooltip", "escstring"]
date: 2018-04-15

description: "Tooltip annotation attached to the head of an edge."
types: ["escString"]
default: "n/a"
min: ""
restrictions: "svg, cmap only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Tooltip annotation attached to the head of an edge. This is used only if the edge has a headURL attribute.
