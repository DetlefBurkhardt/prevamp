---
title: constraint
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "constraint", "bool"]
date: 2018-04-15

description: "If false, the edge is not used in ranking the nodes."
types: ["bool"]
default: "TRUE"
min: ""
restrictions: "dot only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If false, the edge is not used in ranking the nodes.
