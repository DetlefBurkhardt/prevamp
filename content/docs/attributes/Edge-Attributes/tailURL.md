---
title: tailURL
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "tailurl", "escstring"]
date: 2018-04-15

description: "If tailURL is defined, it is output as part of the tail label of the edge."
types: ["escString"]
default: "n/a"
min: ""
restrictions: "svg, map only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If tailURL is defined, it is output as part of the tail label of the edge. Also, this value is used near the tail node, overriding any URL value. See limitation.
