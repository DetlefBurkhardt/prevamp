---
title: decorate
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "decorate", "bool"]
date: 2018-04-15

description: "If true, attach edge label to edge by a 2-segment polyline, underlining the label, then going to the closest point of spline."
types: ["bool"]
default: "FALSE"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If true, attach edge label to edge by a 2-segment polyline, underlining the label, then going to the closest point of spline.
