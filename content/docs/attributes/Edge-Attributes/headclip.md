---
title: headclip
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "headclip", "bool"]
date: 2018-04-15

description: "If true, the head of an edge is clipped to the boundary of the head node; otherwise, the end of the edge goes to the center of the node, or the center of a port, if applicable."
types: ["bool"]
default: "TRUE"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If true, the head of an edge is clipped to the boundary of the head node; otherwise, the end of the edge goes to the center of the node, or the center of a port, if applicable.
