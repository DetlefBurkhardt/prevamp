---
title: headlabel
categories:  ["edge"]
tags: ["tbd", "attribute", "edge", "headlabel", "lblstring"]
date: 2018-04-15

description: "Text label to be placed near head of edge."
types: ["lblString"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Text label to be placed near head of edge. See limitation.
