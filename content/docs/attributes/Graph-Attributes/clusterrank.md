---
title: clusterrank
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "clusterrank", "clustermode"]
date: 2018-04-15

description: "Mode used for handling clusters."
types: ["clusterMode"]
default: "local"
min: ""
restrictions: "dot only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Mode used for handling clusters. If clusterrank is 'local', a subgraph whose name begins with 'cluster' is given special treatment.
