---
title: overlap_shrink
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "overlap_shrink", "bool"]
date: 2018-04-15

description: "If true, the overlap removal algorithm will perform a compression pass to reduce the size of the layout."
types: ["bool"]
default: "TRUE"
min: ""
restrictions: "prism only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If true, the overlap removal algorithm will perform a compression pass to reduce the size of the layout.
