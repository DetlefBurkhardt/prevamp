---
title: model
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "model", "string"]
date: 2018-04-15

description: "This value specifies how the distance matrix is computed for the input graph."
types: ["string"]
default: "shortpath"
min: ""
restrictions: "neato only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

This value specifies how the distance matrix is computed for the input graph.
