---
title: mclimit
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "mclimit", "double"]
date: 2018-04-15

description: "Multiplicative scale factor used to alter the MinQuit (default = 8) and MaxIter (default = 24) parameters used during crossing minimization."
types: ["double"]
default: "1.0"
min: ""
restrictions: "dot only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Multiplicative scale factor used to alter the MinQuit (default = 8) and MaxIter (default = 24) parameters used during crossing minimization. These correspond to the number of tries without improvement before quitting and the maximum number of iterations in each pass.
