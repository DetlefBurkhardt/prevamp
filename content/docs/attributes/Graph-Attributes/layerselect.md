---
title: layerselect
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "layerselect", "layerrange"]
date: 2018-04-15

description: "Selects a list of layers to be emitted."
types: ["layerRange"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Selects a list of layers to be emitted.
