---
title: mosek
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "mosek", "bool"]
date: 2018-04-15

description: "If Graphviz is built with MOSEK defined, mode=ipsep and mosek=true, the Mosek software (www."
types: ["bool"]
default: "FALSE"
min: ""
restrictions: "neato only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If Graphviz is built with MOSEK defined, mode=ipsep and mosek=true, the Mosek software (www.mosek.com) is use to solve the ipsep constraints.
