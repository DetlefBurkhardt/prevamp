---
title: fontnames
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "fontnames", "string"]
date: 2018-04-15

description: "Allows user control of how basic fontnames are represented in SVG output."
types: ["string"]
default: "n/a"
min: ""
restrictions: "svg only"
examples: "crazy.gv"
lastEdit: 2018-04-15
lastEditor: generator
---

Allows user control of how basic fontnames are represented in SVG output. If fontnames is undefined or 'svg', the output will try to use known SVG fontnames. For example, the default font 'Times-Roman' will be mapped to the basic SVG font 'serif'.
