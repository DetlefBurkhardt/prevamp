---
title: colorscheme
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "colorscheme", "string"]
date: 2018-04-15

description: "This attribute specifies a color scheme namespace."
types: ["string"]
default: "n/a"
min: ""
restrictions: ""
examples: "biological.gv"
lastEdit: 2018-04-15
lastEditor: generator
---

This attribute specifies a color scheme namespace.
