---
title: fontcolor
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "fontcolor", "color"]
date: 2018-04-15

description: "Color used for text."
types: ["color"]
default: "black"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Color used for text.
