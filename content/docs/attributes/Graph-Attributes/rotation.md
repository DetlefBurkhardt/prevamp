---
title: rotation
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "rotation", "double"]
date: 2018-04-15

description: "Causes the final layout to be rotated counter-clockwise by the specified number of degrees."
types: ["double"]
default: "0"
min: ""
restrictions: "sfdp only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Causes the final layout to be rotated counter-clockwise by the specified number of degrees.
