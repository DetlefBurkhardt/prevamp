---
title: sep
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "sep", "adddouble", "addpoint"]
date: 2018-04-15

description: "Specifies margin to leave around nodes when removing node overlap."
types: ["addDouble", "addPoint"]
default: "4"
min: ""
restrictions: "not dot"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Specifies margin to leave around nodes when removing node overlap. This guarantees a minimal non-zero distance between nodes.
