---
title: nslimit1
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "nslimit1", "double"]
date: 2018-04-15

description: "nslimit1 is used in computing node x for ranking nodes."
types: ["double"]
default: "n/a"
min: ""
restrictions: "dot only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

nslimit1 is used in computing node x for ranking nodes. If defined, # iterations = nslimit(1) * # nodes; otherwise, # iterations = MAXINT.
