---
title: orientation
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "orientation", "string"]
date: 2018-04-15

description: "If '[lL]*', set graph orientation to landscape Used only if rotate is not defined."
types: ["string"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If '[lL]*', set graph orientation to landscape Used only if rotate is not defined.
