---
title: page
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "page", "double", "point"]
date: 2018-04-15

description: "Width and height of output pages, in inches."
types: ["double", "point"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Width and height of output pages, in inches. If only a single value is given, this is used for both the width and height. At present, this only works for PostScript output. For other types of output, one should use another tool to split the output into multiple output files. Or use the viewport to generate multiple files.
