---
title: fontpath
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "fontpath", "string"]
date: 2018-04-15

description: "Directory list used by libgd to search for bitmap fonts if Graphviz was not built with the fontconfig library."
types: ["string"]
default: "n/a"
min: ""
restrictions: "system-dependent"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Directory list used by libgd to search for bitmap fonts if Graphviz was not built with the fontconfig library. If fontpath is not set, the environment variable DOTFONTPATH is checked. If that is not set, GDFONTPATH is checked. If not set, libgd uses its compiled-in font path. Note that fontpath is an attribute of the root graph.
