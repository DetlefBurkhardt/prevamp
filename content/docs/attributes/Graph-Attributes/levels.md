---
title: levels
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "levels", "int"]
date: 2018-04-15

description: "Number of levels allowed in the multilevel scheme."
types: ["int"]
default: "MAXINT"
min: "0.0"
restrictions: "sfdp only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Number of levels allowed in the multilevel scheme.
