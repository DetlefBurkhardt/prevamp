---
title: layerlistsep
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "layerlistsep", "string"]
date: 2018-04-15

description: "Specifies the separator characters used to split an attribute of type layerRange into a list of ranges."
types: ["string"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Specifies the separator characters used to split an attribute of type layerRange into a list of ranges.
