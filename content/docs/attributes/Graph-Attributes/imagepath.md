---
title: imagepath
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "imagepath", "string"]
date: 2018-04-15

description: "Specifies a list of directories in which to look for image files as specified by the image attribute or using the IMG element in HTML-like labels."
types: ["string"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Specifies a list of directories in which to look for image files as specified by the image attribute or using the IMG element in HTML-like labels.
