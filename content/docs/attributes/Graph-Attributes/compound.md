---
title: compound
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "compound", "bool"]
date: 2018-04-15

description: "If true, allow edges between clusters."
types: ["bool"]
default: "FALSE"
min: ""
restrictions: "dot only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If true, allow edges between clusters. (See lhead and ltail below.)
