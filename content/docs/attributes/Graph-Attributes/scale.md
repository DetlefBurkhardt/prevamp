---
title: scale
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "scale", "double", "point"]
date: 2018-04-15

description: "If set, after the initial layout, the layout is scaled by the given factors."
types: ["double", "point"]
default: "n/a"
min: ""
restrictions: "not dot"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If set, after the initial layout, the layout is scaled by the given factors. If only a single number is given, this is used for both factors.
