---
title: esep
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "esep", "adddouble", "addpoint"]
date: 2018-04-15

description: "Margin used around polygons for purposes of spline edge routing."
types: ["addDouble", "addPoint"]
default: "3"
min: ""
restrictions: "not dot"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Margin used around polygons for purposes of spline edge routing. The interpretation is the same as given for sep. This should normally be strictly less than sep.
