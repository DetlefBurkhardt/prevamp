---
title: viewport
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "viewport", "viewport"]
date: 2018-04-15

description: "Clipping window on final drawing."
types: ["viewPort"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Clipping window on final drawing. Note that this attribute supersedes any size attribute. The width and height of the viewport specify precisely the final size of the output.
