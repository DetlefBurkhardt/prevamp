---
title: searchsize
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "searchsize", "int"]
date: 2018-04-15

description: "During network simplex, maximum number of edges with negative cut values to search when looking for one with minimum cut value."
types: ["int"]
default: "30"
min: ""
restrictions: "dot only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

During network simplex, maximum number of edges with negative cut values to search when looking for one with minimum cut value.
