---
title: remincross
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "remincross", "bool"]
date: 2018-04-15

description: "If true and there are multiple clusters, run crossing minimization a second time."
types: ["bool"]
default: "TRUE"
min: ""
restrictions: "dot only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If true and there are multiple clusters, run crossing minimization a second time.
