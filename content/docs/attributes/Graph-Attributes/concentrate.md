---
title: concentrate
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "concentrate", "bool"]
date: 2018-04-15

description: "If true, use edge concentrators."
types: ["bool"]
default: "FALSE"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If true, use edge concentrators. This merges multiedges into a single edge and causes partially parallel edges to share part of their paths. The latter feature is not yet available outside of dot.
