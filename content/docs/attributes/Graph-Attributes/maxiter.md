---
title: maxiter
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "maxiter", "int"]
date: 2018-04-15

description: "Sets the number of iterations used."
types: ["int"]
default: "100 * # nodes(mode == KK), 200(mode == major), 600(fdp)"
min: ""
restrictions: "fdp, neato only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Sets the number of iterations used.
