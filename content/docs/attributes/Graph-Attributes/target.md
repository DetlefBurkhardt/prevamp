---
title: target
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "target", "escstring", "string"]
date: 2018-04-15

description: "If the object has a URL, this attribute determines which window of the browser is used for the URL."
types: ["escString", "string"]
default: "n/a"
min: ""
restrictions: "svg, map only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If the object has a URL, this attribute determines which window of the browser is used for the URL. See W3C documentation.
