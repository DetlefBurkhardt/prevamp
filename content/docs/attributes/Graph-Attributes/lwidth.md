---
title: lwidth
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "lwidth", "double"]
date: 2018-04-15

description: "Width of graph label in inches."
types: ["double"]
default: "n/a"
min: ""
restrictions: "write only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Width of graph label in inches.
