---
title: dimen
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "dimen", "int"]
date: 2018-04-15

description: "Set the num ber of dimensions used for rendering."
types: ["int"]
default: "2"
min: "2"
restrictions: "sfdp, fdp, neato only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Set the num ber of dimensions used for rendering. The maximum value allowed is 10. If both dimen and dim are set, the latter specifies the dimension used for layout, and the former for rendering. If only dimen is set, this is used for both layout and renderingdimensions.
