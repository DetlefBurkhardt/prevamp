---
title: dim
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "dim", "int"]
date: 2018-04-15

description: "Set the number of dimensions used for the layout."
types: ["int"]
default: "2"
min: "2"
restrictions: "sfdp, fdp, neato only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Set the number of dimensions used for the layout. The maximum value allowed is 10.
