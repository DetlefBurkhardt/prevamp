---
title: outputorder
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "outputorder", "outputmode"]
date: 2018-04-15

description: "Specify order in which nodes and edges are drawn."
types: ["outputMode"]
default: "breadthfirst"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Specify order in which nodes and edges are drawn.
