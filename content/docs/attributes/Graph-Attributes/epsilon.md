---
title: epsilon
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "epsilon", "double"]
date: 2018-04-15

description: "Terminating condition."
types: ["double"]
default: ".0001 * # nodes(mode == KK), .0001(mode == major)"
min: ""
restrictions: "neato only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Terminating condition. If the length squared of all energy gradients are < epsilon, the algorithm stops.
