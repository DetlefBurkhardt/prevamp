---
title: inputscale
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "inputscale", "double"]
date: 2018-04-15

description: "For layout algorithms that support initial input positions (specified by the pos attribute), this attribute can be used to appropriately scale the values."
types: ["double"]
default: "n/a"
min: ""
restrictions: "fdp, neato only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

For layout algorithms that support initial input positions (specified by the pos attribute), this attribute can be used to appropriately scale the values. By default, fdp and neato interpret the x and y values of pos as being in inches. (NOTE: neato -n(2) treats the coordinates as being in points, being the unit used by the layout algorithms for the pos attribute.) Thus, if the graph has pos attributes in points, one should set inputscale=72. This can also be set on the command line using the -s flag flag.
