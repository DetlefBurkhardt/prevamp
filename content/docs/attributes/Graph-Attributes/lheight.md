---
title: lheight
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "lheight", "double"]
date: 2018-04-15

description: "Height of graph label in inches."
types: ["double"]
default: "n/a"
min: ""
restrictions: "write only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Height of graph label in inches.
