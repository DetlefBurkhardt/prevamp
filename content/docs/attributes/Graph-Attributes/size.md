---
title: size
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "size", "double", "point"]
date: 2018-04-15

description: "Maximum width and height of drawing, in inches."
types: ["double", "point"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Maximum width and height of drawing, in inches. If only a single number is given, this is used for both the width and the height.
