---
title: splines
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "splines", "bool", "string"]
date: 2018-04-15

description: "Controls how, and if, edges are represented."
types: ["bool", "string"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Controls how, and if, edges are represented. If true, edges are drawn as splines routed around nodes; if false, edges are drawn as line segments.
