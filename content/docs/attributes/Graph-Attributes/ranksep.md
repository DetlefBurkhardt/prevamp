---
title: ranksep
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "ranksep", "double", "doublelist"]
date: 2018-04-15

description: "In dot, this gives the desired rank separation, in inches."
types: ["double", "doubleList"]
default: "0.5(dot), 1.0(twopi)"
min: "0.02"
restrictions: "twopi, dot only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

In dot, this gives the desired rank separation, in inches. This is the minimum vertical distance between the bottom of the nodes in one rank and the tops of nodes in the next. If the value contains 'equally', the centers of all ranks are spaced equally apart.
