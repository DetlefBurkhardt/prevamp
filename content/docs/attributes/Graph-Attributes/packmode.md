---
title: packmode
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "packmode", "packmode"]
date: 2018-04-15

description: "This indicates how connected components should be packed (cf."
types: ["packMode"]
default: "node"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

This indicates how connected components should be packed (cf. packMode). Note that defining packmode will automatically turn on packing as though one had set pack=true.
