---
title: xdotversion
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "xdotversion", "string"]
date: 2018-04-15

description: "For xdot output, if this attribute is set, this determines the version of xdot used in output."
types: ["string"]
default: "n/a"
min: ""
restrictions: "xdot only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

For xdot output, if this attribute is set, this determines the version of xdot used in output.
