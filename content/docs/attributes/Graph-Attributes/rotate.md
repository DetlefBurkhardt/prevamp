---
title: rotate
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "rotate", "int"]
date: 2018-04-15

description: "If 90, set drawing orientation to landscape."
types: ["int"]
default: "0"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If 90, set drawing orientation to landscape.
