---
title: label_scheme
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "label_scheme", "int"]
date: 2018-04-15

description: "The value indicates whether to treat a node whose name has the form |edgelabel|* as a special node representing an edge label."
types: ["int"]
default: "0"
min: "0"
restrictions: "sfdp only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

The value indicates whether to treat a node whose name has the form |edgelabel|* as a special node representing an edge label. The default (0) produces no effect. If the attribute is set to 1, sfdp uses a penalty-based method to make that kind of node close to the center of its neighbor. With a value of 2, sfdp uses a penalty-based method to make that kind of node close to the old center of its neighbor. Finally, a value of 3 invokes a two-step process of overlap removal and straightening.
