---
title: layersep
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "layersep", "string"]
date: 2018-04-15

description: "Specifies the separator characters used to split the layers attribute into a list of layer names."
types: ["string"]
default: ":\t"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Specifies the separator characters used to split the layers attribute into a list of layer names.
