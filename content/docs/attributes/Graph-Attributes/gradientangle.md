---
title: gradientangle
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "gradientangle", "int"]
date: 2018-04-15

description: "If a gradient fill is being used, this determines the angle of the fill."
types: ["int"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If a gradient fill is being used, this determines the angle of the fill.
