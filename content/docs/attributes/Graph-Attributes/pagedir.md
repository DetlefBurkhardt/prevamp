---
title: pagedir
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "pagedir", "pagedir"]
date: 2018-04-15

description: "If the page attribute is set and applicable, this attribute specifies the order in which the pages are emitted."
types: ["pagedir"]
default: "BL"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If the page attribute is set and applicable, this attribute specifies the order in which the pages are emitted. This is limited to one of the 8 row or column major orders.
