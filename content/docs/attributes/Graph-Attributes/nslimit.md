---
title: nslimit
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "nslimit", "double"]
date: 2018-04-15

description: "Used to set number of iterations in network simplex applications."
types: ["double"]
default: "n/a"
min: ""
restrictions: "dot only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Used to set number of iterations in network simplex applications. If defined, # iterations = nslimit(1) * # nodes; otherwise, # iterations = MAXINT.
