---
title: start
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "start", "starttype"]
date: 2018-04-15

description: "Parameter used to determine the initial layout of nodes."
types: ["startType"]
default: "n/a"
min: ""
restrictions: "fdp, neato only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Parameter used to determine the initial layout of nodes. If unset, the nodes are randomly placed in a unit square with the same seed is always used for the random number generator, so the initial placement is repeatable.
