---
title: levelsgap
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "levelsgap", "double"]
date: 2018-04-15

description: "Specifies strictness of level constraints in neato when mode='ipsep' or 'hier'."
types: ["double"]
default: "0.0"
min: ""
restrictions: "neato only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Specifies strictness of level constraints in neato when mode='ipsep' or 'hier'. Larger positive values mean stricter constraints, which demand more separation between levels. On the other hand, negative values will relax the constraints by allowing some overlap between the levels.
