---
title: mode
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "mode", "string"]
date: 2018-04-15

description: "Technique for optimizing the layout."
types: ["string"]
default: "major"
min: ""
restrictions: "neato only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Technique for optimizing the layout.
