---
title: newrank
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "newrank", "bool"]
date: 2018-04-15

description: "If newrank=true, the ranking algorithm does a single global ranking, ignoring clusters."
types: ["bool"]
default: "FALSE"
min: ""
restrictions: "dot only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If newrank=true, the ranking algorithm does a single global ranking, ignoring clusters. This allows nodes to be subject to multiple constraints. Rank constraints will usually take precedence over edge constraints.
