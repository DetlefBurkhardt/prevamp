---
title: diredgeconstraints
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "diredgeconstraints", "string", "bool"]
date: 2018-04-15

description: "Only valid when mode='ipsep'."
types: ["string", "bool"]
default: "FALSE"
min: ""
restrictions: "neato only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Only valid when mode='ipsep'. If true, constraints are generated for each edge in the largest (heuristic) directed acyclic subgraph such that the edge must point downwards.
