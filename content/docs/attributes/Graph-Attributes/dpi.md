---
title: dpi
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "dpi", "double"]
date: 2018-04-15

description: "This specifies the expected number of pixels per inch on a display device."
types: ["double"]
default: "96.0, 0.0"
min: ""
restrictions: "svg, bitmap output only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

This specifies the expected number of pixels per inch on a display device. For bitmap output, this guarantees that text rendering will be done more accurately, both in size and in placement. For SVG output, it is used to guarantee that the dimensions in the output correspond to the correct number of points or inches.
