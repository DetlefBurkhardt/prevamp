---
title: notranslate
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "notranslate", "bool"]
date: 2018-04-15

description: "By default, the final layout is translated so that the lower-left corner of the bounding box is at the origin."
types: ["bool"]
default: "FALSE"
min: ""
restrictions: "neato only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

By default, the final layout is translated so that the lower-left corner of the bounding box is at the origin. This can be annoying if some nodes are pinned or if the user runs neato -n. To avoid this translation, set notranslate to true.
