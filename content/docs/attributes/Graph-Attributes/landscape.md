---
title: landscape
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "landscape", "bool"]
date: 2018-04-15

description: "If true, the graph is rendered in landscape mode."
types: ["bool"]
default: "FALSE"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If true, the graph is rendered in landscape mode. Synonymous with rotate=90 or orientation=landscape.
