---
title: voro_margin
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "voro_margin", "double"]
date: 2018-04-15

description: "Factor to scale up drawing to allow margin for expansion in Voronoi technique."
types: ["double"]
default: "0.05"
min: "0.0"
restrictions: "not dot"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Factor to scale up drawing to allow margin for expansion in Voronoi technique. dim' = (1+2*margin)*dim.
