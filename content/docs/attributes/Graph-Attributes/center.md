---
title: center
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "center", "bool"]
date: 2018-04-15

description: "If true, the drawing is centered in the output canvas."
types: ["bool"]
default: "FALSE"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

If true, the drawing is centered in the output canvas.
