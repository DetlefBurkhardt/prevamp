---
title: bgcolor
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "bgcolor", "color", "colorlist"]
date: 2018-04-15

description: "When attached to the root graph, this color is used as the background for entire canvas."
types: ["color", "colorList"]
default: "n/a"
min: ""
restrictions: ""
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

When attached to the root graph, this color is used as the background for entire canvas.
