---
title: mindist
categories:  ["graph"]
tags: ["tbd", "attribute", "graph", "mindist", "double"]
date: 2018-04-15

description: "Specifies the minimum separation between all nodes."
types: ["double"]
default: "1.0"
min: "0.0"
restrictions: "circo only"
examples: ""
lastEdit: 2018-04-15
lastEditor: generator
---

Specifies the minimum separation between all nodes.
